<?php
require_once __DIR__ . '/../function/Encode.php';
require_once __DIR__ . '/../function/getDb.php';

class Calendar {

    private $weekStr;
    private $weeks;
    private $holidays;
    private $currentYear;

    private $holidayJs;

    private $start;

    private $changeNum; //0(日曜スタート) or 1(月曜スタート)
    private $changeNum2;//調整用 6 or 0
    private $changeNum3;//調整用 6 or 7


    public function __construct($year, $start) {
        $this->changeNum = $start;
        switch ($start) {
            case 0:
                $this->weekStr = array('日', '月', '火', '水', '木', '金', '土');
                $this->start = "w";
                $this->changeNum2 = 6;
                $this->changeNum3 = 6;
                break;
            case 1:
            default:
                $this->weekStr = array('月', '火', '水', '木', '金', '土', '日');
                $this->start = "N";
                $this->changeNum2 = 0;
                $this->changeNum3 = 7;
        }

        $this->currentYear = $year;
        $db = getDb();
        $sql = "SELECT * FROM holidaytime WHERE date >= ? && date < ?";
        $stt = $db->prepare($sql);
        $stt->bindValue(1, "$year-01-01");
        $stt->bindValue(2, ($year + 1) . "-01-01");
        $stt->execute();
        $db = null;

        while ($row = $stt->fetch()) {
            $this->holidays[] = $row;
        }
        return;
    }

    public function create($mon) {
        $this->weeks = array();

        $year = $this->currentYear;
        $lastDay = date("t", strtotime($year . "-" . $mon . "-1")); // 引数't' 日数[28-31]
        $weekStart = date("$this->start", strtotime($year . "-" . $mon . "-1")); // 引数 'w':曜日数値[0-6] 引数N:[1-7]
        $holidays = $this->holidays;


        $week = "";

            $week .= str_repeat('<td></td>', $weekStart - $this->changeNum);

        $holidayDates = array_column($holidays,'date'); //['date']のみ取り出す
        $holidayNames = array_column($holidays,'name');


        for ($day = 1; $day <= $lastDay; $day ++, $weekStart ++) {
            $datetime = new DateTime("$year-$mon-$day");
            $currentDate = $datetime->format('Y-m-d'); // Y-m-d(2015-03-05)
            $keyNo = array_search($currentDate, $holidayDates); // 戻り値(キーの値/false) 1日は戻り値0なので注意

            if ($keyNo == true || $keyNo === 0) {
                $week .= sprintf('<td class="holiday" onmouseover="showPopup(event,\'%d_%d\');" onmouseout="hidePopup(\'%d_%d\');">%d</td>'
                    ,$mon, $day, $mon, $day, $day); // マウスオーバー処理

                $this->holidayJs[] .= sprintf('<div class="holidayForJs" id=%d_%d>%s</div>'
                    , $mon, $day, $holidayNames[$keyNo]); // 祝日にID付与
            } else {
                $week .= sprintf('<td class="week_class_%d">%d</td>', $weekStart % 7, $day);

            }


            if (($weekStart % 7 == $this->changeNum2) ) {
                $this->weeks[] = '<tr>' . $week . '</tr>';
                $week = '';
            } elseif ($day == $lastDay) {     // 最終週の処理
                $week .= str_repeat('<td></td>', $this->changeNum3 - $weekStart % 7 );
                $this->weeks[] = '<tr>' . $week . '</tr>';
                $week = '';
            }
        }
    }

    public function getWeekStr() {
        return $this->weekStr;
    }

    public function getWeeks() {
        return $this->weeks;
    }

    public function getHolidayJs() {
        return $this->holidayJs;
    }
}
